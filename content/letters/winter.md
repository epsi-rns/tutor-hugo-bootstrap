+++
title  = "Surviving White Winter"
date   = 2015-03-05T08:08:15+07:00
tags   = ["duty", "story"]
categories = ["love"]
+++

It was a frozen winter in cold war era.
We are two men, a boy, two women, a husky, and two shotguns.
After three weeks, we finally configure dzen2.

But we lost our beloved husky before we finally made it.
Now, every january, we remember our husky,
that helped all of us to survive.
