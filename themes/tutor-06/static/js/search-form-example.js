jQuery(function() {
  // Get search results if q parameter is set in querystring
  if (getParameterByName('q')) {
      var query = decodeURIComponent(getParameterByName('q'));
      $("#search_box").val(query);
      
      window.data.then(function(loaded_data){
        $('#site_search').trigger('submit'); 
      });
  }

});

 /* ==========================================================================
    Helper functions
    ========================================================================== */

/**
 * Gets query string parameter - taken from 
 * http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
 * @param {String} name 
 * @return {String} parameter value
 */
function getParameterByName(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}
